# QtSPIM Installation Check Program 2
.text
main:
	lw $t0,data1  # load value of data1 (see .data section) to temporary word register $t0
	lw $t1,data2
	lw $t2,data3

	mul $t3,$t0,$t1  # multiply $t0 * $1, storing result in $t3
	mul $t3,$t3,$t2  # multiply value in $t3 by $t2
	move $a0,$t3  # move $t3 to argument register $a0

	li $v0,1  # print integer value $a0
	syscall
	li $v0,10  # exit program
	syscall

	# variable data used in lines 4-6
	.data
	data1: .word 17
	data2: .word 29
	data3: .word 56