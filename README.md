**Run *test_program1.asm* and *test_program2.asm* in 7 steps:**

1. Ensure the computer has [QtSpim](https://sourceforge.net/projects/spimsimulator/) installed. If it does not, install QtSpim from the given link.

2. Download *test_program1.asm* and *test_program2.asm*. Note note the location of these files on the computer.

3. Open QtSpim and navigate to *File > Load File*. Navigate to test_program1.asm and open the file.

4. Click "Run/Continue", located in the *Simulator* tab or on the toolbar below it.

5. Record the output from Console. If the console is not visible, go to the *Window* tab and ensure "Console" is checked.

6. Navigate to *File > Reinitialize and Load File*. Open test_program2.asm and click "Run/Continue".

7. Record the output from Console. It should be different from the output in Step 5.