# QtSPIM Installation Check Program 1
.text
main:
	li $t0,23  # load 23 to temporary register $t0
	li $t1,35
	li $t2,47

	mul $t3,$t0,$t1  # multiply $t0 * $t1, putting resulting value in $t3
	mul $t3,$t3,$t2  # multiply value in $t3 by $t2
	move $a0,$t3  # move $t3 to argument register $a0 to be used by next syscall

	li $v0,1  # tells syscall what to do: print integer value from $a0
	syscall

	li $v0,10  # tell syscall to exit program
	syscall